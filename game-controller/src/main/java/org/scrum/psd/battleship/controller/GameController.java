package org.scrum.psd.battleship.controller;

import org.scrum.psd.battleship.controller.dto.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class GameController {
    private static Board board = new Board(8);

    public static boolean checkIsHit(Collection<Ship> ships, Position shot) {
        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean checkOutOfBoard(Position shot) {
        return !board.isValid(shot.getColumn()) || !board.isValid(shot.getRow());
    }

    public static List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE));
    }

    public static boolean isShipValid(Ship ship) {
        return ship.getPositions().size() == ship.getSize();
    }

    public static Position getRandomPosition(int size) {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(size)];
        int number = random.nextInt(size);
        Position position = new Position(letter, number);
        return position;
    }

    public static List<Ship> reducePowerFleet(List<Ship> ships, Position shot) {
        if (ships == null) {
            throw new IllegalArgumentException("ships is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : ships) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) if (!ship.isSunk()) ship.setPower(ship.getPower() - 1);
            }
        }

        return ships;
    }

    public static String showEnemyFleetState(Collection<Ship> ships) {
        StringBuilder output = new StringBuilder();
        output.append("\n ##### Current enemy fleet state is: #####");
        for (Ship ship : ships) {
            if (ship.isSunk())
                output.append(String.format("\n%s WITH SIZE OF %d IS SUNK !!!", ship.getName(), ship.getSize()));
            else
                output.append(String.format("\n%s with size of %d is still running...", ship.getName(), ship.getSize()));
        }
        output.append("\n");
        return output.toString();
    }

    public static boolean isFleetAllSunk(List<Ship> fleet){
        boolean allSunk=true;
        for(Ship ship : fleet){
            if(!ship.isSunk()){
                allSunk=false;
            }
        }
        return allSunk;
    }
}
