package org.scrum.psd.battleship.controller.dto;

public class Board {
    private Letter column;
    private int row;

    public Board(int row) {
        this.row = row;
    }

    public Letter getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public boolean isValid(int height) {
        return height > 0 && height <= this.row;
    }

    public boolean isValid(Letter width) {
        switch (width.name()) {
            case "A":
            case "B":
            case "C":
            case "D":
            case "E":
            case "F":
            case "G":
            case "H":
                return true;
        }

        return false;
    }

    public void setColumn(Letter column) {
        this.column = column;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
