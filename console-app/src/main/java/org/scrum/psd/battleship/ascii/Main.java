package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.*;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;

    private static Random random = new Random();

    private final static String[] winningMessages = {"Winner Winner Chicken Dinner", "When you win, say nothing. When you lose, say less.", "You are awesome! Keep up winning!"};
    private final static String[] losingMessages = {"Try better next time!", "You can do it! Try again!", "Better luck next time! Don't give up!"};
    private final static String[] highAccuracyMessages = {"THAT! WAS! AMAZING! Do you even miss?", "Stop using cheat codes, you've hit everything"};
    private final static String[] midAccuracyMessages = {"You did good, but you can do better!", "Come on soldier, pump up that shot accuracy!"};
    private final static String[] lowAccuracyMessages = {"I believe in you, you're the chosen one, now proove that!", "Let's be honest, you can do better, much better!"};

    private static int numberOfTries = 0;
    private static int numberOfHits = 0;

    public static void main(String[] args) {
        init();
    }

    private static void init() {
        System.out.println(colorize("                                     |__", MAGENTA_TEXT()));
        System.out.println(colorize("                                     |\\/", MAGENTA_TEXT()));
        System.out.println(colorize("                                     ---", MAGENTA_TEXT()));
        System.out.println(colorize("                                     / | [", MAGENTA_TEXT()));
        System.out.println(colorize("                              !      | |||", MAGENTA_TEXT()));
        System.out.println(colorize("                            _/|     _/|-++'", MAGENTA_TEXT()));
        System.out.println(colorize("                        +  +--|    |--|--|_ |-", MAGENTA_TEXT()));
        System.out.println(colorize("                     { /|__|  |/\\__|  |--- |||__/", MAGENTA_TEXT()));
        System.out.println(colorize("                    +---------------___[}-_===_.'____                 /\\", MAGENTA_TEXT()));
        System.out.println(colorize("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _", MAGENTA_TEXT()));
        System.out.println(colorize(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7", MAGENTA_TEXT()));
        System.out.println(colorize("|                        Welcome to Battleship                         BB-61/", MAGENTA_TEXT()));
        System.out.println(colorize(" \\_________________________________________________________________________|", MAGENTA_TEXT()));
        System.out.println("");

        String userChoice = selectStartingOption();

        if (selectWinsGame(userChoice)) {
            showCoolWinningMessage();
        } else {
            InitializeGame(selectAutomaticConfiguration(userChoice));

            StartGame();
        }
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\033[2J\033[;H");
        System.out.println("                  __");
        System.out.println("                 /  \\");
        System.out.println("           .-.  |    |");
        System.out.println("   *    _.-'  \\  \\__/");
        System.out.println("    \\.-'       \\");
        System.out.println("   /          _/");
        System.out.println("  |      _  /\" \"");
        System.out.println("  |     /_\'");
        System.out.println("   \\    \\_/");
        System.out.println("    \" \"\" \"\" \"\" \"");

        do {
            boolean wrongInput;
            boolean outOfBoard;
            boolean isEnemyHit;
            boolean amIHit;
            Position position = null;

            System.out.println(colorize("\n======================================================\n", BOLD()));
            System.out.println("Player, it's your turn.");

            do {
                wrongInput = false;
                System.out.print(colorize("Enter coordinates for your shot :", WHITE_BACK(), BLACK_TEXT()));
                System.out.print(" ");

                try {
                    position = parsePosition(scanner.next());
                } catch (IllegalArgumentException ex) {
                    wrongInput = true;
                }
                numberOfTries++;

                outOfBoard = wrongInput || GameController.checkOutOfBoard(position);
                isEnemyHit = !outOfBoard && GameController.checkIsHit(enemyFleet, position);


                if (outOfBoard) {
                    System.out.println(colorize("The coordinates were out of the board range!\n", ITALIC()));
                }
            } while (outOfBoard);

            if (isEnemyHit) {
                numberOfHits++;
                beep();

                printExplosion();

                enemyFleet = GameController.reducePowerFleet(enemyFleet, position);
            } else {
                printSplash();
            }

            System.out.println(isEnemyHit ? colorize("\n\tYeah ! Nice HIT !", GREEN_TEXT()) : colorize("\n\tMISS !", RED_TEXT()));

            boolean playerWon = GameController.isFleetAllSunk(enemyFleet);

            if (playerWon) {
                showCoolWinningMessage();
                startAgain();
                return;
            }

            System.out.println(colorize(GameController.showEnemyFleetState(enemyFleet)));

            position = getRandomPosition();
            amIHit = GameController.checkIsHit(myFleet, position);
            System.out.println(colorize("\n------------------------------------------------------\n", BOLD()));
            System.out.print(String.format(colorize("Computer shot in %s%s and ", amIHit ? RED_TEXT() : GREEN_TEXT()), position.getColumn(), position.getRow()));
            System.out.println(amIHit ? colorize("HIT your ship!", RED_TEXT(), BOLD()) : colorize("MISSED your ships!", GREEN_TEXT(), BOLD()));

            if (amIHit) {
                beep();
                printExplosion();
            }
            boolean computerWon = GameController.isFleetAllSunk(myFleet);
            if (computerWon) {
                showCoolLosingMessage();
                startAgain();
                return;
            }
        } while (true);
    }

    private static void printSplash() {
        System.out.println(colorize("            _           _     ", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("           | |         | |    ", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("  ___ _ __ | | __ _ ___| |__  ", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize(" / __| '_ \\| |/ _` / __| '_ \\ ", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize(" \\__ \\ |_) | | (_| \\__ \\ | | |", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize(" |___/ .__/|_|\\__,_|___/_| |_|", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("     | |                      ", BLUE_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("     |_|                      ", BLUE_TEXT(), SLOW_BLINK()));
    }

    private static void beep() {
        System.out.println("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        return new Position(letter, number);
    }

    private static void InitializeGame(boolean automaticPlayerInitialization) {
        if (automaticPlayerInitialization) {
            automaticallyInitializePlayerFleet();
        } else {
            InitializeMyFleet();
        }

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        System.out.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet) {
            System.out.println("");
            System.out.printf("Please enter the positions for the %s (size: %s)%n", ship.getName(), ship.getSize());
            for (int i = 1; i <= ship.getSize(); i++) {
                System.out.printf("Enter position %s of %s (i.e A3): ", i, ship.getSize());

                String positionInput = scanner.next();
                ship.addPosition(positionInput);
            }
        }
    }

    private static void startAgain() {
        System.out.println("Do you want to try again? (y/n)");

        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();

        if (input.toLowerCase(Locale.ROOT).equals("n")) {
            System.out.println("Good bye!");
            System.exit(1);
        } else if (input.toLowerCase(Locale.ROOT).equals("y")) {
            init();
        }
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static String selectStartingOption() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose what you want to do");
        System.out.println("[configure]");
        System.out.println("[win]");
        System.out.println("rest: manual input");


        return scanner.next();
    }

    private static void automaticallyInitializePlayerFleet() {
        myFleet = GameController.initializeShips();

        myFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        myFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        myFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        myFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        myFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        myFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        myFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        myFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        myFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        myFleet.get(2).getPositions().add(new Position(Letter.A, 2));
        myFleet.get(2).getPositions().add(new Position(Letter.B, 2));
        myFleet.get(2).getPositions().add(new Position(Letter.C, 2));

        myFleet.get(3).getPositions().add(new Position(Letter.F, 5));
        myFleet.get(3).getPositions().add(new Position(Letter.G, 5));
        myFleet.get(3).getPositions().add(new Position(Letter.H, 5));

        myFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        myFleet.get(4).getPositions().add(new Position(Letter.C, 6));
    }

    private static boolean selectAutomaticConfiguration(String userChoice) {
        return userChoice.toLowerCase().equals("configure");
    }

    private static boolean selectWinsGame(String userChoice) {
        return userChoice.toLowerCase().equals("win");
    }

    private static void printExplosion() {
        System.out.println(colorize("     \\         .  ./", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("   \\      .:\" \";'.:..\" \"   /", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("       (M^^.^~~:.'\" \").", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize(" -   (/  .    . . \\ \\)  -", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("    ((| :. ~ ^  :. .|))", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize(" -   (\\- |  \\ /  |  /)  -", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("      -\\  \\     /  /-", YELLOW_TEXT(), SLOW_BLINK()));
        System.out.println(colorize("        \\  \\   /  /", YELLOW_TEXT(), SLOW_BLINK()));
    }

    private static void showCoolWinningMessage() {
        System.out.println();
        System.out.println(colorize("_//        _//_//_///     _//_///     _//_////////_///////", RAPID_BLINK()));
        System.out.println(colorize("_//        _//_//_/ _//   _//_/ _//   _//_//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_//   _/   _//_//_// _//  _//_// _//  _//_//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_//  _//   _//_//_//  _// _//_//  _// _//_//////  _/ _//", RAPID_BLINK()));
        System.out.println(colorize("_// _/ _// _//_//_//   _/ _//_//   _/ _//_//      _//  _//", RAPID_BLINK()));
        System.out.println(colorize("_/ _/    _////_//_//    _/ //_//    _/ //_//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_//        _//_//_//      _//_//      _//_////////_//      _//", RAPID_BLINK()));
        System.out.println();

        int winningIndex = random.nextInt(winningMessages.length);
        System.out.println(colorize(winningMessages[winningIndex], CYAN_TEXT()));
        showPlayerAccuracy();
    }

    private static void showCoolLosingMessage() {
        System.out.println();
        System.out.println(colorize("_//          _////       _// //  _////////_///////", RAPID_BLINK()));
        System.out.println(colorize("_//        _//    _//  _//    _//_//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_//      _//        _// _//      _//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_//      _//        _//   _//    _//////  _/ _//", RAPID_BLINK()));
        System.out.println(colorize("_//      _//        _//      _// _//      _//  _//", RAPID_BLINK()));
        System.out.println(colorize("_//        _//     _// _//    _//_//      _//    _//", RAPID_BLINK()));
        System.out.println(colorize("_////////    _////       _// //  _////////_//      _//", RAPID_BLINK()));
        System.out.println();

        int losingIndex = random.nextInt(losingMessages.length);
        System.out.println(colorize(losingMessages[losingIndex], CYAN_TEXT()));
        showPlayerAccuracy();
    }

    private static void showPlayerAccuracy() {
        if (numberOfTries == 0) { numberOfTries = 1; }
        double accuracy = numberOfHits / numberOfTries;
        if (accuracy < 0.5) {
            int lowAcuracyMsg = random.nextInt(lowAccuracyMessages.length);
            System.out.println(colorize("Your accuracy was below 50%. "+lowAccuracyMessages[lowAcuracyMsg], RED_TEXT()));
        } else if (accuracy < 1) {
            int midAcuracyMsg = random.nextInt(midAccuracyMessages.length);
            System.out.println(colorize("Your accuracy was above 50%, but it's not perfect. "+midAccuracyMessages[midAcuracyMsg], YELLOW_TEXT()));
        } else {
            int highAcuracyMsg = random.nextInt(highAccuracyMessages.length);
            System.out.println(colorize("Congrats, your accuracy was 100%. " + highAccuracyMessages[highAcuracyMsg], GREEN_TEXT()));
        }
        numberOfTries=0;
        numberOfHits=0;
    }
}
